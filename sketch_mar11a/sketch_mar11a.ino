#include <dht.h>


/************************************************************
  Variáveis Globais - Estufa Controlada
 ************************************************************/
dht my_dht_1;
dht my_dht_2;
#define dht_pin_5 5
#define dht_pin_6 6
#define vcc_pin A3
#define gnd_pin 12
#define gnd2_pin 13 
#define solo_pin_A0 A0
#define pH_pin_A1 A1
int pin_8 = 8;
int pin_9 = 9;
int pin_10 = 10;
int count = 0;
int count2 = 0;
int count3 = 0;
int count4 = 0;
int delay1 = 10000;
int delay2 = 20000;
bool irrigacao = false;
bool arrefecimento = false;
float umidade_ar = 0x00;
float percentual_umidade = 0x00;
float temperatura_informada = 0x00;
float umidade_ar_informada = 0x00;
float media_temperatura_sensores = 0x00;
float media_umidade_ar_sensores = 0x00;


/************************************************************
  Sensor Temperatura/Umidade 1 - Estufa Controlada
 ************************************************************/
float temperatura_5 = 0x00;
float umidade_5     = 0x00;

/************************************************************
  Sensor Temperatura/Umidade 2 - Estufa Controlada
 ************************************************************/
float temperatura_6 = 0x00;
float umidade_6     = 0x00;

/************************************************************
  Sensor Solo 1 - Estufa Controlada
 ************************************************************/
float solo_A0 = 0x00;
float umidade_solo = 0x00;

/************************************************************
  Sensor pH - Estufa Controlada
 ************************************************************/
float pH = 0x00;
float pH_A1 = 0x00;

void setup() {
  Serial.begin(9600);
  pinMode(pin_8, OUTPUT);
  pinMode(pin_9, OUTPUT);
  pinMode(pin_10, OUTPUT);
  digitalWrite(pin_8, HIGH);
  digitalWrite(pin_9, HIGH);
  digitalWrite(pin_10, HIGH);

}

void loop() {
/************************************************************
   Estufa 1 - Estufa Controlada
************************************************************/
my_dht_1.read11(dht_pin_5);
my_dht_2.read11(dht_pin_6);

/*Sensor 1 Temperatura*/
temperatura_5 = my_dht_1.temperature;
Serial.println("E1S1T");
delay(delay1);
Serial.println(temperatura_5);
delay(delay2);
  
/*Sensor 1 Umidade*/
umidade_5 = my_dht_1.humidity;
Serial.println("E1S1U");
delay(delay1);
Serial.println(umidade_5);
delay(delay2);

/*Sensor 2 Temperatura*/
temperatura_6 = my_dht_2.temperature;
Serial.println("E1S2T");
delay(delay1);
Serial.println(temperatura_6);
delay(delay2);

/*Sensor 2 Umidade*/
umidade_6     = my_dht_2.humidity;
Serial.println("E1S2U");
delay(delay1);
Serial.println(umidade_6);
delay(delay2);

/*Sensor 1 Solo*/
Serial.println("E1S1S");
delay(delay1);
solo_A0 = analogRead(solo_pin_A0);
percentual_umidade = (solo_A0 * 100)/1023;
percentual_umidade = 100 - percentual_umidade;
Serial.println(percentual_umidade);
delay(delay2);

/*Sensor pH*/
Serial.println("E1SpH");
delay(delay1);
pH_A1 = analogRead(pH_pin_A1);
pH = pH_A1 * (5.0 / 1024.0);
pH = 7 + ((2.5 - pH) / 0.50);
Serial.println(pH,1);
delay(delay2);

/*Arresfriamento*/
media_temperatura_sensores = (temperatura_5 + temperatura_6) / 2;
if (count > 10 || count == 0){
  //Arrefecimento
  Serial.println("E1ARF");
  delay(delay1);
  Serial.println("Dados");
  delay(delay1);
  if(Serial.available()>0){
    int caractere = Serial.parseInt();
    delay(delay1);
    temperatura_informada = float(caractere)*5;
  }
  Serial.println("Fim dados");
  delay(delay1);
  Serial.println(temperatura_informada);
  delay(delay1);
  count = 0; 
}
if(temperatura_informada < media_temperatura_sensores){
  if(irrigacao == true){
    digitalWrite(pin_10, HIGH);
  }
    digitalWrite(pin_8, LOW);
    digitalWrite(pin_9, LOW);
    arrefecimento = true;
}else{
  digitalWrite(pin_8, HIGH);
  digitalWrite(pin_9, HIGH);
  arrefecimento = false;
}

/*Irrigação*/
media_umidade_ar_sensores = (umidade_5 + umidade_6)/2;
if (count > 10 || count == 0){
  //Irrigação
  Serial.println("E1IRR");
  delay(delay1);
  Serial.println("UARI");
  delay(delay1);
  Serial.println("Dados");
  delay(delay1);
  if(Serial.available()>0){
    int caractere = Serial.parseInt();
    delay(delay1);
    umidade_ar_informada = float(caractere)*5;
  }
  Serial.println("Fim dados");
  delay(delay1);
  Serial.println(umidade_ar_informada);
  delay(delay1);
  
  Serial.println("USLI");
  delay(delay1);
  Serial.println("Dados");
  delay(delay1);
  if(Serial.available()>0){
    int caractere = Serial.parseInt();
    delay(delay1);
    umidade_solo = float(caractere)*5;
  }
  Serial.println("Fim dados");
  delay(delay1);
  Serial.println(umidade_solo);
  delay(delay1);
}

if(irrigacao == true){
  if(count2 == 2){
    digitalWrite(pin_10, HIGH);
    irrigacao = false;
  }else{
    digitalWrite(pin_10, LOW);
    delay(delay1);
    count2 += 1;
  }
}else{
  if(count3 == 20){
    if(umidade_solo > percentual_umidade){
      if(arrefecimento == true){
        digitalWrite(pin_10, HIGH);
      }else{
        digitalWrite(pin_10, LOW);
        delay(delay1);
        irrigacao = true;
        count2 = 0;
      }
    }else if(umidade_ar_informada > media_umidade_ar_sensores){
      if(arrefecimento == true){
        digitalWrite(pin_10, HIGH);
      }else{
        digitalWrite(pin_10, LOW);
        delay(delay1);
        irrigacao = true;
        count2 = 0;
      }
    }else{
      digitalWrite(pin_10, HIGH);
      irrigacao = false;
    }
    count3 = 0;
  }else{
    count3+= 1;
  }
}
count+= 1;
}
 
