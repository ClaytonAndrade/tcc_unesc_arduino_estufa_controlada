import requests
import json
import urllib.request
import serial
import struct
import time

#*********************************************************************************************
# Variaveis globais
#*********************************************************************************************
port_1 = '/dev/ttyUSB0'

comunicacaoSerial = serial.Serial(port_1, 9600)

url = 'http://10.0.0.115:8080/'
headers={'Content-type':'application/json', 'Accept':'application/json'}

#*********************************************************************************************
# Variaveis Estufa 1
#*********************************************************************************************
val = 0
Estufa_1 = 1
identificacaoSensorEnum_1 = 0 #pH
identificacaoSensorEnum_2 = 1 #solo 1
identificacaoSensorEnum_3 = 4 #temperatura 1
identificacaoSensorEnum_4 = 5 #umidade 1
identificacaoSensorEnum_5 = 6 #temperatura 2
identificacaoSensorEnum_6 = 7 #umidade 2

#*********************************************************************************************
# Metodos
#*********************************************************************************************
#busca na base via get configuracao sensor temperatura e umidade
def buscarConfigSensorTemperaturaUmidade(idSensor):
    endpoint= "sensor-temperatura-umidade/"
    try:
        r = requests.get(url+endpoint+idSensor)
        print(r.status_code)
        return r.json() 
    except:
        print("Servidor offline")
        return 0

#busca na base via get configuracao sensor solo
def buscarConfigSensorSolo(idSensor):
    endpoint= "sensor-solo/"
    try:
        r = requests.get(url+endpoint+idSensor)
        print(r.status_code)
        return r.json()
    except:
        print("Servidor offline")

#busca na base via get configuracao sensor pH
def buscarConfigSensorPH(idSensor):
    endpoint= "sensor-ph/"
    try:
        r = requests.get(url+endpoint+idSensor)
        print(r.status_code)
        return r.json()
    except:
        print("Servidor offline")

#busca na base via get ultimo registro
def buscarUltimo():
    endpoint= "leitura/max"
    try:
        r = requests.get(url+endpoint)
        print (req.status_code)
        return r.json()
    except:
        print("Servidor offline")

#persiste na base via post
def inserir(valorObtido, tipoLeitura, Estufa, identificacaoSensorEnum):
    endpoint= "leitura"
    data = {"valorObtido":valorObtido, 
            "tipoLeitura":tipoLeitura,
            "estufaModel":
                {"idEstufa":Estufa},
            "identificacaoSensorEnum":identificacaoSensorEnum
            }
    try:
        req = requests.post(url+endpoint, data=json.dumps(data),headers=headers)
        print (req.status_code)
        return req
    except:
        print("Servidor offline")

def leituDadosAuxiliar(val):
    if(val <= 5):
        comunicacaoSerial.write(b'1')
    elif(val <= 10):
        comunicacaoSerial.write(b'2')
    elif(val <= 15):
        comunicacaoSerial.write(b'3')
    elif(val <= 20):
        comunicacaoSerial.write(b'4')
    elif(val <= 25):
        comunicacaoSerial.write(b'5')
    elif(val <= 30):
        comunicacaoSerial.write(b'6')
    elif(val <= 35):
        comunicacaoSerial.write(b'6')
    elif(val <= 40):
        comunicacaoSerial.write(b'8')
    elif(val <= 45):
        comunicacaoSerial.write(b'9')
    elif(val <= 50):
        comunicacaoSerial.write(b'10')
    elif(val <= 55):
        comunicacaoSerial.write(b'11')
    elif(val <= 60):
        comunicacaoSerial.write(b'12')
    elif(val <= 65):
        comunicacaoSerial.write(b'13')
    elif(val <= 70):
        comunicacaoSerial.write(b'14')
    elif(val <= 75):
        comunicacaoSerial.write(b'15')
    elif(val <= 80):
        comunicacaoSerial.write(b'16')
    elif(val <= 85):
        comunicacaoSerial.write(b'17')
    elif(val <= 90):
        comunicacaoSerial.write(b'18')
    elif(val <= 95):
        comunicacaoSerial.write(b'19')
    elif(val <= 100):
        comunicacaoSerial.write(b'20')

#*********************************************************************************************
# Execucao das requisicoes
#*********************************************************************************************

while (True):
    valorRecebido = comunicacaoSerial.readline()
    valorRecebido = valorRecebido[0:-2] 
    print(valorRecebido.decode('UTF-8'))

    if (valorRecebido.decode('UTF-8') == 'E1S1T'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_1, identificacaoSensorEnum_3)
        inserir(valorRecebido, "Temperatura S1", Estufa_1, identificacaoSensorEnum_3)
        continue
     
    if (valorRecebido.decode('UTF-8') == 'E1S1U'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_1, identificacaoSensorEnum_4)
        inserir(valorRecebido, "Umidade Ar S1", Estufa_1, identificacaoSensorEnum_4)
        continue
    
    if (valorRecebido.decode('UTF-8') == 'E1S2T'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_1, identificacaoSensorEnum_5)
        inserir(valorRecebido, "Temperatura S2", Estufa_1, identificacaoSensorEnum_5)
        continue
     
    if (valorRecebido.decode('UTF-8') == 'E1S2U'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_1, identificacaoSensorEnum_6)
        inserir(valorRecebido, "Umidade Ar S2", Estufa_1, identificacaoSensorEnum_6)
        continue
    
    if (valorRecebido.decode('UTF-8') == 'E1S1S'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_1, identificacaoSensorEnum_2)
        inserir(valorRecebido, "Umidade solo", Estufa_1, identificacaoSensorEnum_2)
        continue
    
    if (valorRecebido.decode('UTF-8') == 'E1SpH'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        valorRecebido = valorRecebido.decode('UTF-8')
        print(valorRecebido, Estufa_1, identificacaoSensorEnum_1)
        inserir(valorRecebido, "pH Água", Estufa_1, identificacaoSensorEnum_1)
        continue
    
    if (valorRecebido.decode('UTF-8') == 'E1ARF'):
        response = buscarConfigSensorTemperaturaUmidade('1')
        if(response != 0):
            configuracaoValor = response["valorInformado"]-(response["percentualVariacao"]*response["valorInformado"])
        response = buscarConfigSensorTemperaturaUmidade('3')
        if(response != 0):
            configuracaoValor2 = response["valorInformado"]-(response["percentualVariacao"]*response["valorInformado"])
            mediaValor = (configuracaoValor + configuracaoValor2)/2
            val = int(mediaValor)
            valorRecebido = comunicacaoSerial.readline()
            valorRecebido = valorRecebido[0:-2] 
            print(valorRecebido.decode('UTF-8'))
            if (valorRecebido.decode('UTF-8') == 'Dados'):
                leituDadosAuxiliar(val)
        
        print(comunicacaoSerial.readline())
        print(comunicacaoSerial.readline())
        continue
    
    if (valorRecebido.decode('UTF-8') == 'E1IRR'):
        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        print(valorRecebido.decode('UTF-8'))
        if(valorRecebido.decode('UTF-8') == 'UARI'):
            response = buscarConfigSensorTemperaturaUmidade('2')
            print(response["valorInformado"])
            if(response != 0):
                configuraçãoValor = response["valorInformado"]-(response["percentualVariacao"]*response["valorInformado"])
            response = buscarConfigSensorTemperaturaUmidade('4')
            print(response["valorInformado"])
            if(response != 0):
                configuraçãoValor2 = response["valorInformado"]-(response["percentualVariacao"]*response["valorInformado"])
                mediaValor = (configuraçãoValor + configuraçãoValor2)/2
                val = int(mediaValor)
                print(val)
                valorRecebido = comunicacaoSerial.readline()
                valorRecebido = valorRecebido[0:-2] 
                print(valorRecebido.decode('UTF-8'))
                if (valorRecebido.decode('UTF-8') == 'Dados'):
                    leituDadosAuxiliar(val)
                
            print(comunicacaoSerial.readline())
            print(comunicacaoSerial.readline())

        valorRecebido = comunicacaoSerial.readline()
        valorRecebido = valorRecebido[0:-2]
        print(valorRecebido.decode('UTF-8'))
        if(valorRecebido.decode('UTF-8') == 'USLI'):
            response = buscarConfigSensorSolo('1')
            print(response["valorInformado"])
            if(response != 0):
                configuraçãoValor = response["valorInformado"]-(response["percentualVariacao"]*response["valorInformado"])
                val = int(configuraçãoValor)
                valorRecebido = comunicacaoSerial.readline()
                valorRecebido = valorRecebido[0:-2] 
                print(valorRecebido.decode('UTF-8'))
                if (valorRecebido.decode('UTF-8') == 'Dados'):
                    leituDadosAuxiliar(val)
                
            print(comunicacaoSerial.readline())
            print(comunicacaoSerial.readline())
        continue